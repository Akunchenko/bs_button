/*
  LoRa Duplex communication

  Sends a message every half second, and polls continually
  for new incoming messages. Implements a one-byte addressing scheme,
  with 0xFF as the broadcast address.

  Uses readString() from Stream class to read payload. The Stream class'
  timeout may affect other functuons, like the radio's callback. For an

  created 28 April 2017
  by Tom Igoe
*/
#include <SPI.h>              // include libraries
#include <LoRa.h>

const uint8_t csPin = 17;          // LoRa radio chip select
const uint8_t resetPin = 16;       // LoRa radio reset
const uint8_t irqPin = 3;         // change for your board; must be a hardware interrupt pin

String outgoing;              // outgoing message

unsigned int msgCount = 0;            // count of outgoing messages
byte localAddress = 0xBB;     // address of this device
byte destination = 0xFF;      // destination to send to
long lastSendTime = 0;        // last send time
int interval = 2000;          // interval between sends

void setup() {
  Serial.begin(115200);                   // initialize serial
  pinMode(9, OUTPUT);
  
  while (!Serial);
  digitalWrite(9, LOW);
  Serial.println("LoRa Duplex");
  // override the default CS, reset, and IRQ pins (optional)
  LoRa.setPins(csPin, resetPin, irqPin);// set CS, reset, IRQ pin

  while (!LoRa.begin(433E6)) {             // initialize ratio at 915 MHz
    LoRa.end();
    Serial.println("LoRa init failed. Check your connections.");
//    while (true) {
      digitalWrite(9, HIGH);  // if failed, do nothing
//    }
  }
  LoRa.setTxPower(20);
  LoRa.setSpreadingFactor(10);
  //  Serial.println("LoRa init succeeded.");
}

void loop() {
  if (millis() - lastSendTime > interval) {
    String message = "HeLoRa World!";   // send a message
    sendMessage(message);
    //    Serial.println("Sending " + message);
    lastSendTime = millis();            // timestamp the message
    interval = 100;    // 2-3 seconds
  }

  // parse for a packet, and call onReceive with the result:
  onReceive(LoRa.parsePacket());
}

void sendMessage(String outgoing) {
  LoRa.beginPacket();                   // start packet
  LoRa.write(destination);              // add destination address
  LoRa.write(localAddress);             // add sender address
  LoRa.write(msgCount & 0xFF);          // add message ID part 2
  LoRa.write(msgCount >> 8);            // add message ID part 1
  LoRa.write(outgoing.length());        // add payload length
  LoRa.print(outgoing);                 // add payload
  LoRa.endPacket();                     // finish packet and send it
  msgCount++;                           // increment message ID
}

void onReceive(int packetSize) {
  if (packetSize == 0) return;          // if there's no packet, return

  // read packet header bytes:
  int recipient = LoRa.read();          // recipient address
  byte sender = LoRa.read();            // sender address
  byte incomingMsgId = LoRa.read();     // incoming msg ID
  byte incomingLength = LoRa.read();    // incoming msg length

  String incoming = "";

  while (LoRa.available()) {
    incoming += (char)LoRa.read();
  }

  if (incomingLength != incoming.length()) {   // check length for error
    //    Serial.println("error: message length does not match length");
    return;                             // skip rest of function
  }

  // if the recipient isn't this device or broadcast,
  if (recipient != localAddress && recipient != 0xFF) {
    //    Serial.println("This message is not for me.");
    return;                             // skip rest of function
  }

  //  // if message is for this device, or broadcast, print details:
  //  Serial.println("Received from: 0x" + String(sender, HEX));
  //  Serial.println("Sent to: 0x" + String(recipient, HEX));
  //  Serial.println("Message ID: " + String(incomingMsgId));
  //  Serial.println("Message length: " + String(incomingLength));
  //  Serial.println("Message: " + incoming);
  //  Serial.println("RSSI: " + String(LoRa.packetRssi()));
  //  Serial.println("Snr: " + String(LoRa.packetSnr()));
  //  Serial.println();
}
