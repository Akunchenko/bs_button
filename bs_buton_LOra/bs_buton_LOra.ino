/*
 Name:		bs_buton_LOra.ino
 Created:	29.01.2020 9:48:08
 Author:	akunchenko
*/
	#include <LowPower.h>
#include <SPI.h>              // include libraries
#include <LoRa.h>
#include <avr/sleep.h>
#include <avr/wdt.h> 
const uint8_t csPin = 17;          // LoRa radio chip select
const uint8_t resetPin = 16;       // LoRa radio reset
const uint8_t irqPin = 3;         // change for your board; must be a hardware interrupt pin

String outgoing;              // outgoing message

unsigned int msgCount = 0;            // count of outgoing messages
const uint8_t localAddress = 0xBB;     // address of this device
const uint8_t destination = 0xFF;      // destination to send to
long lastSendTime = 0;        // last send time
int interval = 2000;          // interval between sends
const uint8_t wakeUpPin = 2;
const float R1 = 300;
const float R2 = 150;
const float dividerRatio = (R1 + R2) / R2;
const float voltPerUnit = 1.1 / 1024;
//����������� �������� �������� � ����������
const float voltRatio = voltPerUnit * dividerRatio;

const uint8_t pin = A5;
#define NUM_SAMPLES 10

int sum = 0;                    // sum of samples taken
unsigned char sample_count = 0; // current sample number
float voltage = 0.0;
void setup() {
	//Serial.begin(115200);                   // initialize serial
	analogReference(INTERNAL1V1);
	PreparePeriphery();
	//SetAllPinsLow();
	analogReference(DEFAULT);
	pinMode(9, OUTPUT);
	pinMode(wakeUpPin, INPUT_PULLUP);
	
	//Serial.println("LoRa Duplex");
	// override the default CS, reset, and IRQ pins (optional)
	LoRa.setPins(csPin, resetPin, irqPin);// set CS, reset, IRQ pin
	
	while (!LoRa.begin(433E6)) {             // initialize ratio at 433 MHz
		LoRa.end();
		//Serial.println("LoRa init failed. Check your connections.");
		
		digitalWrite(9, HIGH);  // if failed, do nothing
		delay(500);
		digitalWrite(9, LOW);  // if failed, do nothing
	}
	attachInterrupt(0, wakeUp, LOW);
	LoRa.setSpreadingFactor(7);
}

void loop() {
	
	digitalWrite(9, HIGH);
	// Turn power on to ADC
	PRR &= ~(1 << PRADC);

	// Enable ADC
	ADCSRA |= (1 << ADEN);

	while (sample_count < NUM_SAMPLES) {
		sum += analogRead(A0);
		sample_count++;
		delay(5);
	}
	sum = analogRead(A0);
	
	voltage = sum * voltRatio;
	String message = String(voltage);
	sendMessage(message);
	sendMessage(voltage);
	digitalWrite(9, LOW);
	ADCSRA = 0;

	// Disable power to ADC
	PRR |= (1 << PRADC);
	
	goToSleep();

}

void sendMessage(uint8_t outgoing) {
	LoRa.idle();
	LoRa.beginPacket();                   // start packet
	LoRa.write(destination);              // add destination address
	LoRa.write(localAddress);             // add sender address
	LoRa.write(msgCount & 0xFF);          // add message ID part 2
	LoRa.write(msgCount >> 8);            // add message ID part 1
	LoRa.write(sizeof(outgoing));        // add payload length
	LoRa.write(outgoing);                 // add payload
	LoRa.endPacket();                     // finish packet and send it
	msgCount++;                           // increment message ID
}
void sendMessage(String outgoing) {
	LoRa.idle();
	LoRa.beginPacket();                   // start packet
	LoRa.write(destination);              // add destination address
	LoRa.write(localAddress);             // add sender address
	LoRa.write(msgCount & 0xFF);          // add message ID part 2
	LoRa.write(msgCount >> 8);            // add message ID part 1
	LoRa.write(outgoing.length());        // add payload length
	LoRa.print(outgoing);                 // add payload
	LoRa.endPacket();                     // finish packet and send it
	msgCount++;                           // increment message ID
}
void onReceive(int packetSize) {
	if (packetSize == 0) return;          // if there's no packet, return

	// read packet header bytes:
	int recipient = LoRa.read();          // recipient address
	byte sender = LoRa.read();            // sender address
	byte incomingMsgId = LoRa.read();     // incoming msg ID
	byte incomingLength = LoRa.read();    // incoming msg length

	String incoming = "";

	while (LoRa.available()) {
		incoming += (char)LoRa.read();
	}

	if (incomingLength != incoming.length()) {   // check length for error
	  //    Serial.println("error: message length does not match length");
		return;                             // skip rest of function
	}

	// if the recipient isn't this device or broadcast,
	if (recipient != localAddress && recipient != 0xFF) {
		//    Serial.println("This message is not for me.");
		return;                             // skip rest of function
	}

	//  // if message is for this device, or broadcast, print details:
	//  Serial.println("Received from: 0x" + String(sender, HEX));
	//  Serial.println("Sent to: 0x" + String(recipient, HEX));
	//  Serial.println("Message ID: " + String(incomingMsgId));
	//  Serial.println("Message length: " + String(incomingLength));
	//  Serial.println("Message: " + incoming);
	//  Serial.println("RSSI: " + String(LoRa.packetRssi()));
	//  Serial.println("Snr: " + String(LoRa.packetSnr()));
	//  Serial.println();
}
void SetAllPinsLow() {
	for (byte i = 0; i <= A5; i++){
		pinMode(i, OUTPUT);    // changed as per below
		digitalWrite(i, LOW);  //     ditto
	}
}
void PreparePeriphery() {
	
	ADCSRA = 0;
	// Disable power to I2C, TIM2, TIM1, and ADC
	PRR = bit(PRTWI)  |    // TWI (I2C)
		bit(PRTIM2)   |   // Timer/Counter2
		bit(PRTIM1)	  |   // Timer/Counter1
		bit(PRUSART0) |
		(1 << PRADC);     // ADC*/

}
void wakeUp() {
	cli();
	detachInterrupt(0);
	// Just a handler for the pin interrupt.
}

void goToSleep() {
	cli();
	// Disable interrupts while we configure sleep
	ADCSRA &= ~(1 << ADEN); // ��������� ���
	LoRa.sleep();
	attachInterrupt(0, wakeUp, LOW);
	set_sleep_mode(SLEEP_MODE_PWR_DOWN); //������������� ������������ ��� �����
	sleep_enable();
	// ��������� �������� ����������� ���������� ������� 
	MCUCR = bit(BODS) | bit(BODSE);
	MCUCR = bit(BODS);
	sei();
	sleep_cpu(); // ��������� �� � ������ �����
	sleep_disable();

}
